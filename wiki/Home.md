# Welcome

MoCheQoS is a bounded model checker for quality of service (QoS) properties of distributed message-passing systems. It exploits features of [ChorGram](https://bitbucket.org/eMgssi/stable_chorgram/wiki), a tool chain to support choreographic development of message-oriented applications.

- The instructions to install MoCheQoS are [here](installation.md)
- Basic usages of MoCheQoS is described [here](usage.md) (please follow these instructions to perform a "quick check")
- The experiments considered in the paper are [here](experiments.md)
- The syntax of the input files of MoCheQoS are [here](syntax.md)

Note that it is not necessary to know the [syntax](syntax.md) of the input files of MoCheQoS in order to reproduce our experiments.
