# Syntax of the input files

Strings are made of any character which is not a separator; the current list of separators is

```bash
\n\t @.,;:()[]{}|+-*/^!?%&#
```
	
Names of participants, machines, or systems must start with a capital letter.
 
## Specifying CFSMs

The `fsa` format (after *f*inite *s*tate *a*utomata) is a textual format for specifying communicating systems consisting of a number of CFSMS. For instance,

```ABNF
   .outputs A
   .state graph
   q0 1 ! hello q1
   q0 1 ! world q1
   .marking q0
   .end
   
   .outputs B
   .state graph
   q0 0 ? hello q1
   q0 0 ? world q1
   .marking q0
   .end
```
specifies a system of two machines. Each line describes (some element of) a CFSM:

- machine `0` is named `A` (line `.outputs A`), has `q0` as initial state (line `.marking q0`) from which it may send either a message `hello` or a message `world` to machine `1` and move to state `q1` (lines `q0 1 ! hello q1` and `q0 1 ! world q1`, respectively). 
- machine `1` is name `B` (line `.outputs B`) ready to receive either message sent by machine `0`.

Machine names are used to denote the channels in the files generated by the tool (for instance, the first transition of `A` is displayed as `AB ! hello` rather than `01!hello`). Lines `.outputs ...`, `.state graph`, `.marking ...`, and `.end` are mandatory. Lines starting with `--` are considered comments and therefore ignored. Messages are strings; the current implementation does not interpret them, but in the future we plan to fix a syntax and a semantics in order to deal with e.g., variables, binderds, values, etc.

## Specifying QoS-extended CFSMs

The `qosfsa` format extends the `fsa` format with QoS information. For instance,

```ABNF
fsa{
   .outputs A
   .state graph
   q0 1 ! hello q1
   q0 1 ! world q1
   .marking q0
   .end
   
   .outputs B
   .state graph
   q0 0 ? hello q1
   q0 0 ? world q1
   .marking q0
   .end
}
qos_attributes{
   cost:+,
   mem:max
}
qos_specifications{
   A@q0: (and (<= mem 10) (= cost (* mem 2))),
   B@q1: (and (= mem 5) (= cost 0))
}
final_states{
   A: [q1],
   B: [q1]
}
```
extends the system above with QoS specifications. Each section describes some element of the qos-extended system:

- the `fsa` section describes the system of CFSMs as in the `fsa` format;
- the `qos_attributes` section specifies the QoS attributes of the system; the attribute named `cost` has an aggregation operator `+` (line `cost:+`), while the attribute named `mem` has an aggregation operator `max`(line `mem:max`); the aggregation operator symbol has to be a valid SMT-LIB binary operator;
- the `qos_specifications` section specifies the QoS specifications attached to states of the system; the state `q0` of machine `A` has a QoS specification stating the relation between the value of the attribute `mem` and the value of the attribute `cost` (line `A@q0: (and (<= mem 10) (= cost (* mem 2)))`); likewise, the state `q1` of machine `B` has a QoS specification stating the values of the attributes `mem` and `cost` (line `B@q1: (and (= mem 5) (= cost 0))`); the QoS specification is an SMT-LIB term where the only free variables are attribute names; states that do not appear in the list of QoS specifications do not have any QoS constraint associated to them and the values for their QoS attributes will not be considered during the analysis (e.g., the state `q1` of machine `A` and the state `q0` of machine `B`);
- the `final_states` section specifies the final states of the system as a list of states for each machine; the state `q1` of machine `A` and the state `q1` of machine `B` are final states (lines `A: [q1]` and `B: [q1]`); models of the system are runs that contain a final configuration, i.e., a configuration where all the machines are in a final state.

## Syntax of Global Choreographies
The syntax of g-choreographies (aka global graphs) is obtained by blending together and sugaring the syntaxes presented in various papers (barred the construct 'break'). The grammar of g-choreographies is:

```ABNF
   G ::= (o)                                  * empty graph
      |  Ptp -> Ptp : str                     * interaction
	  |  break                                * exit a loop
	  |  accept                               * marks accepting points of participants
      |  G '|' G                              * fork
      |  G ';' G                              * sequential
      |  'sel' Ptp '{' G + ... + G '}'        * choice
      |  'repeat' P '{' G '}'                 * loop
      |  '{' G '}'
```

where `(o)` is the empty graph (trailing occurrences of `(o)` are omitted). A `break` statement exits a loop; it can occur only in branch of a choice wrapped in a loop and it is controlled by an explicitely specified participant (e.g., `repeat X { ... break ....}` is valid, while `repeat { ... break ....}` is not). 
The binary operators `\_|\_`, and `\_;\_` are given in ascending order of precedence.

A g-choreography representing the ping-pong protocol in this syntax could be

```python
   sel {
      Ping -> Pong : finished
      +
      repeat Ping {
      Ping -> Pong : ping ;
      Pong -> Ping : pong
      };
      Ping -> Pong : finished
   }
```

### Syntax of QoS-extended Global Choreographies

The `qosgc` format extends the `gc` format with QoS information. For instance,

```python
   sel {
      Ping -> Pong : finished;
      accept
      +
      repeat Ping {
      Ping -> Pong : ping {sqos: (and (<= mem 10) (= cost (* mem 2)))};
      Pong -> Ping : pong {rqos: (and (= mem 5) (= cost 0))}
      };
      Ping -> Pong : finished;
      accept
   }
   qos{cost:+, mem:max}
```
extends the g-choreography above with QoS specifications. Each interaction of the form `Ptp -> Ptp : str` can contain QoS information between curly brackets inside `str`, by using the keywords `sqos`, `sqos'`, `rqos`, `rqos'`, which respectively denote the state of the sender before the ouput action, the state of the sender after the output action, the state of the receiver before the input action, and the state of the receiver after the input action. For example, the interaction `Ping -> Pong : ping {sqos: (and (<= mem 10) (= cost (* mem 2)))}` states the QoS specification of the sender before the output action, while the interaction `Pong -> Ping : pong {rqos: (and (= mem 5) (= cost 0))}` states the QoS specification of the receiver after the input action. The accepting points marked by the keyword `accept` are trated as final states of the machines. The `qos` section specifies the QoS attributes of the system together with their aggregation operators.

### Syntax of QL formulas

The `.ql` format is a textual format for specifying QL formulas. The grammar of QL formulas is:

```ABNF
   QL ::= True             * atomic truth value
      |  T                 * atomic truth value (synonim of True)
      |  qos{ smt }        * atomic qos formula
      |  Not QL            * negation
      |  ¬ QL              * negation (synonim of Not)
      |  QL Or QL          * disjunction
      |  QL v QL           * disjunction (synonim of Or)
      |  QL Until [G] QL   * until modality
      |  QL U [G] QL       * until modality (synonim of Until)
```
where `smt` is an SMT-LIB term where the only free variables are attribute names and `G` is a g-choreography.

For instance,

```python
   qos{ (and (<= mem 10) (= cost (* mem 2))) } 
   Until [
      Ping -> Pong : ping;
      Pong -> Ping : pong;
      Ping -> Pong : finished
   ] 
   qos{ (and (= mem 5) (= cost 0)) }
```
is a QL formula stating that there exists a point in the future that matches the g-choreography, where the aggregated values of `mem` and `cost` should satisfy the second qos formula, and that up to that point, the aggregated values of `mem` and `cost` should satisfy the first qos formula.